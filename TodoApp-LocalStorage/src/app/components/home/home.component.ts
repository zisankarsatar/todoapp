import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { bindCallback } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  data = {
    pendings:[
      "Get to work",
      "Pick up groceries",
      "Go home",
      "Fail asleep",
    ],
  
    inProsess:[
      "Get up",
      "Bruth teeth",
      "Walk dog",
    ],
  
    done:[
      "Get up",
      "Bruth teeth",
      "Take a shower",
      "Check e-mail",
      "Walk dog"
    ]
  };
  
  constructor() {
    this.setItems();
  }

  ngOnInit(): void {
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
                        Object.keys(this.data).forEach((key) =>{
                          localStorage.setItem(key,JSON.stringify(this.data[key]));
                        });                
    }
  }

  addTodo(todo){
    this.data.pendings.push(todo.value);
    todo.value=" ";
    localStorage.setItem("pendings", JSON.stringify(this.data.pendings));
  }

  setItems(){
    Object.keys(this.data).forEach((key) =>{
      if(!localStorage.getItem(key)){
        localStorage.setItem(key,JSON.stringify(this.data[key]));
      }else{
        this.data[key]=JSON.parse(localStorage.getItem(key));
      }
    });
  }
}
